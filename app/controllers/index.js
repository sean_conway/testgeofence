function doClick(e) {
	var geoFence = require('google.geofence');
	_ = require("alloy/underscore")._, alert($.label.text);

	geoFence.addEventListener('enterregions', function(e) {
		var regions = JSON.parse(e.regions);
		Ti.API.info("number of fences: " + regions.length);

		_.each(regions, function(region) {

			Ti.API.info("Entering geofence: " + JSON.stringify(region.identifier));

		});
	});

	geoFence.addEventListener('exitregions', function(e) {
		var regions = JSON.parse(e.regions);
		_.each(regions, function(region) {

			Ti.API.info("Exiting geofence: " + JSON.stringify(region.identifier));

		});
	});

	geoFence.addEventListener('monitorregions', function(e) {
		Ti.API.info("Monitoring regions: " + JSON.stringify(e));
	});

	var fences = [{

		"center" : {

			latitude : 55.62509823,

			longitude : -111.87053167

		},

		identifier : "test",

		radius : 50

	}];

	geoFence.startMonitoringForRegions(JSON.stringify(fences));

}

$.index.open();
